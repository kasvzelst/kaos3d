﻿using OpenGLRenderer.Renderables;
using OpenTK.Windowing.Desktop;
using System;
using System.Threading;

namespace OpenGLRenderer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting rendering thread");

            Console.WriteLine("Started Render thread");

            var nativeWindowSettings = new NativeWindowSettings()
            {
                Size = new OpenTK.Mathematics.Vector2i(1280, 720),
                Title = "Kaos3D - Prototype"
            };

            using (var window = new Window(GameWindowSettings.Default, nativeWindowSettings))
            {
                window.Run();
            }
        }
    }
}
